![ |small ](../imgs/rabbitmq.png)

## Running the rabbitmq-server service

The rabbitmq service comes installed with the aiida environment. You can verify 
the commands for the service are in your path with the following command:

```
which rabbitmq-server
```

To start the service in detached (as background process) mode issue the following:

```
  rabbitmq-server -detached
```

Check that rabbitmq-server is running:

```
ps aux | grep $USER | grep rabbit
```

Rabbitmq will keep a log of activity and lib info in `~/$USER/.rabbitmq/`

To stop the rabbitmq server you have to directly kill the process. Use the 
following one liner to do so: 

```
kill -KILL $(ps axo user:20,pid,comm |  grep -E "beam.smp" | awk -F ' ' '{print $2}')
```

