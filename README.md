![ |small ](imgs/aiida.png)
# AiiDA on Midway 

Instructions for starting services to use the Automated Interactive Infrastructure and 
Database for Computational Science [(AiiDA)](https://aiida-core.readthedocs.io/en/latest/) 
framework on midway. These instructions are for running the AiiDA core service version 1.1.2
and its dependent services as a regular user from the login nodes. For instructions on setup and
use of older versions of AiiDA, see the archive notes at the end of this document. 


## Accessing Aiida on Midway

*Note* The AiiDA software is installed in a conda environment within the Anaconda3/2019.03 module.
If you have set up your default environment to use a specific python module you must first unload 
any python module before loading the AiiDA module so that there is no conflict. Furthermore if you
have installed any python packages locall using pip, be aware that they could be a possible source
of conflict with using AiiDA. 

To make the aiida core services and dependent services accessible within your shell environment load
the following Anaconda3 module and activate the aiida-1.2.1 conda environment. 

``` 
module load Anaconda3/2019.03
conda activate aiida-1.2.1
```

Verify that the aiida_core service verdi is functional by running issuing following:

```
verdi profile list
```

This should return that no  profile exists. If instead it barfs up python errors about the
"entrypoint cache", then you can try issuing the following to fix this:

```
reentry scan
```

## Setting up and Starting Dependent Services 

AiiDA relies on two dependent services, postgresql and rabbitmq, which are installed within the conda environment, but require
some additional setup and activation. 

### Setting up Postgresql and Running the Database

See the [postgresql setup instructions](postgresql/README.md).

*Note* You will later need to provide information about the postgresql db name you created and the
user account and password chosen, when you run verdi setup to configure aiida. 

### Starting the rabbitmq-server service

See the [starting rabbitmq service instructions](rabbitmq/README.md). 


## Configuring AiiDA

You must next configure aiida. You will specify the database name and user account
that was previously configured for the postgresql db setup. You need to set the 
TZ environment variable prior to running the verdi setup. If using midway the  
server is based in Chicago so one would accordingly set the following: 

```
export TZ='America/Chicago'
```

To initiate the aiida setup process issue the following:

```
  verdi setup
```
When prompted for the Database engine, choose the default postgresql_psycopg2. 
When prompted for the Database backend, choose the default django. 

At this point, if you have not experienced any errors, you should be able to
proceed with starting the verdi daemon.

*Note* AiiDA stores the configuration for each profile in `~/.aiida/config.json` should 
you need to directly modify or remove a profile setting. 

Start the verdi daemon
```
  verdi daemon start
```
Status of verdi daemon

```
  verdi daemon status
  verdi plugin list aiida.calculations
```

You will see there are a minimal set of plugins. You'll likely have to add the ones
you need. There is an [AiiDA plugin registry](https://aiidateam.github.io/aiida-registry/) where
you can search for available plugins.  Alternatively from the CLI you can search with pip: 

```
  pip search aiida
```

Currently you must use pip to install AiiDA plugins. For example to install the quantumespresso plugin you would issue the following:

```
pip install aiida-quantumespresso --pre
```

To stop the verdi daemon issue the following: 
```
   verdi daemon stop
```

If you are tearing down all services, you'll additionally need to stop the rabbitmq and psql services. See the corresponding pages for starting each service above to locate the information on stopping the service. 

## Archive Notes on Installation and Setup for AiiDA versions <= 1.0 
See the [Version 1.0 instructions in the archive](archive/README.md).

