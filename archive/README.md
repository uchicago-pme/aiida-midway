![ |small ](imgs/aiida.png)
# Installation and setup of AiiDA versions <= 1.0 on Midway

*Note* This material is now obsolete.

Instructions for setting up and using the Automated Interactive Infrastructure and 
Database for Computational Science [(AiiDA)](https://aiida-core.readthedocs.io/en/latest/) 
framework on midway. These instructions are for running the aiida service version 1.0.0b3
and its dependent services as a regular user from the login nodes. 


## Setting up AiiDA core python library and dependent services

*Note* These instructions are for python2. AiiDA core is not functional, even with
the developer version targeting python3 from their developers channel. Best to wait
till they have a stable production release of aiida_core for python3 before bothering 
with this. 


### Installing aiida_core python package

You'll need to load a python2 module. Here we use the Anaconda2 distribution of python and
create a user spceific conda environment named `aiida`. If you have already created a 
conda environment and installed aiida_core then simply `source activate <your-aiida-env>`
and proceed to the Setting up postgresql. 
 
``` 
module load Anaconda2/2019.03
conda create --name aiida python=2.7.15
source activate aiida
pip install --pre aiida
```
Confirm aiida_core is functional by running the following. It should return that no 
profile exists. If instead it barfs up python errors, then you need to fix your aiida_core
install. 
```
verdi profile list
```


### Setting up Postgresql and Creating DB and Users

See the [postgresql setup instructions](postgresql/README.md).


### Running the rabbitmq-server service

See the [starting rabbitmq service instructions](rabbitmq/README.md). 


## Configuring AiiDA

You must next configure aiida. To start the setup process issue the following:

```
  verdi setup
```
You will need to provide information about the postgresql db name you created and the
user account and password. 
There are other backend configuartion choices that need to be set one of which is 
whether to use django or pysqlalchemy. Only pysqlalchemy appears to be functional
so do NOT choose django when configuring. 
 
At this point, if you have not experienced any errors, you should be able to
proceed with starting the verdi daemon.

Start the verdi daemon
```
  verdi daemon start
```
Status of verdi daemon

```
  verdi daemon status
  verdi plugin list aiida.calculations
```

You will see there are a minimal set of plugins. You'll likely have to add the ones
you need. There is an [AiiDA plugin registry](https://aiidateam.github.io/aiida-registry/) where
you can search for available plugins.  Alternatively from the CLI you can search with pip: 

```
  pip search aiida
```

Currently you must use pip to install AiiDA plugins. For example to install the quantumespresso plugin you would issue the following:

```
pip install aiida-quantumespresso --pre
```

