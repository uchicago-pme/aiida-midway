![ |small ](../imgs/psql-logo.jpg)

##  PostgreSQL service setup 
 
 The postgresql database comes installed with the aiida environment, but you will 
 still need to start the service and setup both a database and user accounts.
 The following set of instructions explains how to do this on midway. It assumes
 that postgresql is already accessible within your shell environment. 
 
1.) Create a folder for all the postgres related configuration and log 
information in your home folder. ~/.postgresql
   
```
export PDIR=$HOME/.postgresql
mkdir -p $PDIR
cd $PDIR
```
  
2.) Initialize the postgres db setup from within the $PDIR directory. 

```
initdb -D .
```

3.) Start the postgresql db service. 
*Note* if you wish to use a port other than the default port, you need to pass 
the -p flag followed by the port number to use  (  -p <port-number> ). 

```
pg_ctl -D . -l logfile start
```

The log of the postgres server process will be contained in `~/.postgresql/logfile`


4.) Create the aiida user and the aiida db. In the example below the db is named aiidadb. You can use any
name you wish, but then you need to ensure this db name is used when setting up the
aiida configuration.
See page https://www.postgresql.org/docs/9.6/app-createdb.html for more info on db creation. 

```
createuser -P -d -a -e aiida
createdb --owner='aiida' --encoding='UTF-8' --lc-collate='en_US.UTF-8' --lc-ctype='en_US.UTF-8' --template='template0' -e  aiidadb
```

5.) Connect to the db to test is setup correctly

```
psql -h localhost -d aiidadb -U aiida -W  
```

At this point your postgresql setup is setup for aiida to use. 

To stop the db service issue the following:
 
```
pg_ctl -D . -m smart stop
```
